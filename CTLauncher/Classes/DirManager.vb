﻿Imports System.IO

Public Class DirManager
	''' <summary>
	''' Returns the NT folder, regardless of whether it is valid.
	''' </summary>
	''' <returns></returns>
	Public Shared Property NuclearThroneDirectory() As String
		Get
			Return My.Settings.ntfolder
		End Get
		Set(ByVal value As String)
			My.Settings.ntfolder = value
		End Set
	End Property

	''' <summary>
	''' Checks the validity of the currently selected NT folder. Returns True if everything is okay.
	''' </summary>
	''' <returns></returns>
	Public Shared Function ValidNTFolder() As Boolean
		Return ValidNTFolder(My.Settings.ntfolder)
	End Function

	''' <summary>
	''' Checks the validity of the specified NT folder. Returns True if everything is okay.
	''' </summary>
	''' <returns></returns>
	Public Shared Function ValidNTFolder(folder As String) As Boolean
		'Check if Empty
		If folder Is Nothing OrElse folder = "" Then Return False
		'Check if Exists
		If Not Directory.Exists(folder) Then Return False
		'Check Executable
		If Not File.Exists(Path.Combine(folder, "nuclearthrone.exe")) And Not File.Exists(Path.Combine(folder, "customthrone.exe")) Then Return False
		Return True
	End Function

	''' <summary>
	''' Returns the path to the custom throne executable, regardless of whether it exists or not.
	''' </summary>
	''' <returns></returns>
	Public Shared Function CustomThroneExecutable() As String
		Return Path.Combine(My.Settings.ntfolder, "customthrone.exe")
	End Function

	''' <summary>
	''' Returns the path to the custom throne directory (../nt/ct/), regardless of whether it exists or not.
	''' </summary>
	''' <returns></returns>
	Public Shared Function CustomThroneDirectory() As String
		Return Path.Combine(My.Settings.ntfolder, "customthrone")
	End Function

	''' <summary>
	''' Returns a temporary directory, regardless of whether it exists or not.
	''' </summary>
	''' <returns></returns>
	Public Shared Function TempDirectory() As String
		Return Path.Combine(Path.GetTempPath, "customthrone\")
	End Function
End Class
