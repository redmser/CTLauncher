﻿Public Class ModPack
	Public Sub New()
	End Sub

	Public Sub New(internalname As String, displayname As String)
		Me.InternalName = internalname
		Me.DisplayName = displayname
	End Sub

	Private _displayname As String = ""
	''' <summary>
	''' Name of the modpack as it should be displayed in the dropdown.
	''' </summary>
	''' <returns></returns>
	Public Property DisplayName() As String
		Get
			If _displayname = "" Then
				Return Me.InternalName
			End If
			Return _displayname
		End Get
		Set(ByVal value As String)
			_displayname = value
		End Set
	End Property

	Private _internalname As String = ""
	''' <summary>
	''' Name of the folder that the modpack should be located in.
	''' </summary>
	''' <returns></returns>
	Public Property InternalName() As String
		Get
			Return _internalname
		End Get
		Set(ByVal value As String)
			_internalname = value
		End Set
	End Property

	''' <summary>
	''' Returns the full path to this mod's directory.
	''' </summary>
	''' <returns></returns>
	Public Function FullPath() As String
		Return IO.Path.Combine(DirManager.CustomThroneDirectory, Me.InternalName)
	End Function

	Public Overrides Function ToString() As String
		If Me.InternalName = "default" Then Return "(Default)"
		If Me.DisplayName <> Me.InternalName Then
			Return Me.DisplayName & " (" & Me.InternalName & ")"
		End If
		Return Me.DisplayName
	End Function
End Class
