﻿Imports System.Net

Public Class TimedWebClient
	Inherits WebClient

	''' <summary>
	''' Time in milliseconds until timeout
	''' </summary>
	Public Property Timeout() As Integer
		Get
			Return _timeout
		End Get
		Set
			_timeout = Value
		End Set
	End Property
	Private _timeout As Integer

	''' <summary>
	''' Creates a WebClient that times out after 30 seconds.
	''' </summary>
	Public Sub New()
		Me.New(30000)
	End Sub

	''' <summary>
	''' Creates a new WebClient that times out after the specified amount of milliseconds.
	''' </summary>
	''' <param name="timeout"></param>
	Public Sub New(timeout As Integer)
		Me.Timeout = timeout
	End Sub

	Protected Overrides Function GetWebRequest(address As Uri) As WebRequest
		Dim request = MyBase.GetWebRequest(address)
		If request IsNot Nothing Then
			request.Timeout = Me.Timeout
		End If
		Return request
	End Function
End Class