﻿Public Class UpdateResult
	Public Sub New(latestver As String, dllink As String, snapver As String, snapdl As String, apply As Boolean)
		Me.LatestVersion = latestver
		Me.DownloadLink = dllink
		Me.ApplyUpdate = apply
		Me.SnapshotVersion = snapver
		Me.SnapshotDownload = snapdl
	End Sub

	Private _latestversion As String
	''' <summary>
	''' The latest version number, as a string.
	''' </summary>
	''' <returns></returns>
	Public Property LatestVersion() As String
		Get
			Return _latestversion
		End Get
		Set(ByVal value As String)
			_latestversion = value
		End Set
	End Property

	Private _dllink As String
	''' <summary>
	''' The download link to the updated version.
	''' </summary>
	''' <returns></returns>
	Public Property DownloadLink() As String
		Get
			Return _dllink
		End Get
		Set(ByVal value As String)
			_dllink = value
		End Set
	End Property

	Private _apply As Boolean
	''' <summary>
	''' Apply the update without asking the user.
	''' </summary>
	''' <returns></returns>
	Public Property ApplyUpdate() As Boolean
		Get
			Return _apply
		End Get
		Set(ByVal value As Boolean)
			_apply = value
		End Set
	End Property

	Private _snapver As String
	Public Property SnapshotVersion As String
		Get
			Return _snapver
		End Get
		Set(ByVal value As String)
			_snapver = value
		End Set
	End Property

	Private _snapdl As String
	Public Property SnapshotDownload As String
		Get
			Return _snapdl
		End Get
		Set(ByVal value As String)
			_snapdl = value
		End Set
	End Property
End Class
