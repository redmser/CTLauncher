﻿Public Enum UseSOptions
	''' <summary>
	''' The number should not be returned if the amount is "1".
	''' </summary>
	NoNumberForOne = 1
End Enum

Public Class Utils
	''' <summary>
	''' Determines whether the string should be singular or plural depending on the count.
	''' </summary>
	''' <param name="cnt">Number of objects</param>
	''' <param name="str">String to pluralize</param>
	''' <param name="options">One or more options (specify multiple using "Or")</param>
	''' <returns></returns>
	Public Shared Function UseS(cnt As Long, str As String, Optional options As UseSOptions = 0) As String
		Dim pluralS = "s"
		If cnt = 1 Then pluralS = ""

		'Return without count
		If options And UseSOptions.NoNumberForOne Then
			Return str & pluralS
		End If

		Return cnt & " " & str & pluralS
	End Function
End Class
