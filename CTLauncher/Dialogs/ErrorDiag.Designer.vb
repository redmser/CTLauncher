﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ErrorDiag
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ErrorDiag))
		Me.OK = New System.Windows.Forms.Button()
		Me.Label1 = New System.Windows.Forms.Label()
		Me.errorlog = New System.Windows.Forms.TextBox()
		Me.SuspendLayout()
		'
		'OK
		'
		Me.OK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.OK.DialogResult = System.Windows.Forms.DialogResult.OK
		Me.OK.Location = New System.Drawing.Point(233, 302)
		Me.OK.Name = "OK"
		Me.OK.Size = New System.Drawing.Size(75, 27)
		Me.OK.TabIndex = 0
		Me.OK.Text = "OK"
		Me.OK.UseVisualStyleBackColor = True
		'
		'Label1
		'
		Me.Label1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
			Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Label1.AutoSize = True
		Me.Label1.Location = New System.Drawing.Point(2, 8)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(314, 39)
		Me.Label1.TabIndex = 1
		Me.Label1.Text = "An error has occured. Don't panic, though!" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "If you wish to report this as a bug, " &
	"send me the information below." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "You are free to continue using the program like " &
	"normal."
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopCenter
		'
		'errorlog
		'
		Me.errorlog.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
			Or System.Windows.Forms.AnchorStyles.Left) _
			Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.errorlog.Location = New System.Drawing.Point(8, 56)
		Me.errorlog.Multiline = True
		Me.errorlog.Name = "errorlog"
		Me.errorlog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
		Me.errorlog.Size = New System.Drawing.Size(300, 241)
		Me.errorlog.TabIndex = 2
		'
		'ErrorDiag
		'
		Me.AcceptButton = Me.OK
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(317, 336)
		Me.Controls.Add(Me.errorlog)
		Me.Controls.Add(Me.Label1)
		Me.Controls.Add(Me.OK)
		Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
		Me.MaximizeBox = False
		Me.MinimizeBox = False
		Me.Name = "ErrorDiag"
		Me.ShowInTaskbar = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
		Me.Text = "Error"
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub

	Friend WithEvents OK As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents errorlog As TextBox
End Class
