﻿Public Class ErrorDiag
    Public Sub New(errormsg As String)
        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        errorlog.Text = errormsg
    End Sub

    Private Sub errorlog_Click(sender As Object, e As EventArgs) Handles errorlog.Click
        errorlog.SelectAll()
    End Sub
End Class