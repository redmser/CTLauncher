﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class LaunchPending
	Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		Try
			If disposing AndAlso components IsNot Nothing Then
				components.Dispose()
			End If
		Finally
			MyBase.Dispose(disposing)
		End Try
	End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
	Private Sub InitializeComponent()
		Me.Label1 = New System.Windows.Forms.Label()
		Me.ButtonLaunchNow = New System.Windows.Forms.Button()
		Me.ButtonWait = New System.Windows.Forms.Button()
		Me.SuspendLayout()
		'
		'Label1
		'
		Me.Label1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
			Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Label1.Location = New System.Drawing.Point(72, 8)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(180, 28)
		Me.Label1.TabIndex = 0
		Me.Label1.Text = "Custom Throne will launch once the update check is finished..."
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopCenter
		'
		'ButtonLaunchNow
		'
		Me.ButtonLaunchNow.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.ButtonLaunchNow.DialogResult = System.Windows.Forms.DialogResult.Cancel
		Me.ButtonLaunchNow.ForeColor = System.Drawing.SystemColors.WindowFrame
		Me.ButtonLaunchNow.Image = Global.CTLauncher.My.Resources.Resources.application_go
		Me.ButtonLaunchNow.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.ButtonLaunchNow.Location = New System.Drawing.Point(208, 77)
		Me.ButtonLaunchNow.Name = "ButtonLaunchNow"
		Me.ButtonLaunchNow.Size = New System.Drawing.Size(111, 27)
		Me.ButtonLaunchNow.TabIndex = 1
		Me.ButtonLaunchNow.Text = "Launch now!"
		Me.ButtonLaunchNow.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
		Me.ButtonLaunchNow.UseVisualStyleBackColor = True
		'
		'ButtonWait
		'
		Me.ButtonWait.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.ButtonWait.DialogResult = System.Windows.Forms.DialogResult.OK
		Me.ButtonWait.ForeColor = System.Drawing.SystemColors.WindowFrame
		Me.ButtonWait.Image = Global.CTLauncher.My.Resources.Resources.arrow_refresh
		Me.ButtonWait.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.ButtonWait.Location = New System.Drawing.Point(8, 77)
		Me.ButtonWait.Name = "ButtonWait"
		Me.ButtonWait.Size = New System.Drawing.Size(111, 27)
		Me.ButtonWait.TabIndex = 2
		Me.ButtonWait.Text = "Wait for Update"
		Me.ButtonWait.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
		Me.ButtonWait.UseVisualStyleBackColor = True
		'
		'LaunchPending
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(327, 111)
		Me.Controls.Add(Me.ButtonWait)
		Me.Controls.Add(Me.ButtonLaunchNow)
		Me.Controls.Add(Me.Label1)
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
		Me.Name = "LaunchPending"
		Me.ShowInTaskbar = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
		Me.Text = "Launch Pending"
		Me.ResumeLayout(False)

	End Sub

	Friend WithEvents Label1 As Label
	Friend WithEvents ButtonLaunchNow As Button
	Friend WithEvents ButtonWait As Button
End Class
