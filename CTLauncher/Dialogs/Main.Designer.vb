﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Main
	Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		Try
			If disposing AndAlso components IsNot Nothing Then
				components.Dispose()
			End If
		Finally
			MyBase.Dispose(disposing)
		End Try
	End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
	Private Sub InitializeComponent()
		Me.components = New System.ComponentModel.Container()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Main))
		Me.ToolTip = New System.Windows.Forms.ToolTip(Me.components)
		Me.modpack = New System.Windows.Forms.ComboBox()
		Me.EarlyAccess = New System.Windows.Forms.CheckBox()
		Me.NTFolder = New System.Windows.Forms.TextBox()
		Me.UpdateLaunch = New System.Windows.Forms.ComboBox()
		Me.LaunchSettings = New System.Windows.Forms.ComboBox()
		Me.SteamInGame = New System.Windows.Forms.CheckBox()
		Me.ButtonLaunch = New System.Windows.Forms.Button()
		Me.ButtonUpdate = New System.Windows.Forms.Button()
		Me.ButtonImportMP = New System.Windows.Forms.Button()
		Me.ButtonRemoveMPs = New System.Windows.Forms.Button()
		Me.ButtonCreateMP = New System.Windows.Forms.Button()
		Me.ButtonSaveSettings = New System.Windows.Forms.Button()
		Me.ButtonDetect = New System.Windows.Forms.Button()
		Me.ButtonBrowse = New System.Windows.Forms.Button()
		Me.UpdateCheckInterrupt = New System.Windows.Forms.ComboBox()
		Me.ButtonReloadMPs = New System.Windows.Forms.Button()
		Me.WebBrowser = New System.Windows.Forms.WebBrowser()
		Me.Label1 = New System.Windows.Forms.Label()
		Me.Tabs = New System.Windows.Forms.TabControl()
		Me.TabNews = New System.Windows.Forms.TabPage()
		Me.TabManageMods = New System.Windows.Forms.TabPage()
		Me.ListModPacks = New System.Windows.Forms.ListView()
		Me.ColumnActive = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
		Me.ColumnName = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
		Me.ColumnPath = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
		Me.ColumnAuthor = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
		Me.ContextMenuList = New System.Windows.Forms.ContextMenuStrip(Me.components)
		Me.CreateNewToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.RemoveSelectedToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.ImportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.ReloadToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.Label3 = New System.Windows.Forms.Label()
		Me.TabSettings = New System.Windows.Forms.TabPage()
		Me.Label6 = New System.Windows.Forms.Label()
		Me.Label5 = New System.Windows.Forms.Label()
		Me.Label4 = New System.Windows.Forms.Label()
		Me.Label2 = New System.Windows.Forms.Label()
		Me.StatusStrip = New System.Windows.Forms.StatusStrip()
		Me.StatusText = New System.Windows.Forms.ToolStripStatusLabel()
		Me.StatusSpring = New System.Windows.Forms.ToolStripStatusLabel()
		Me.StatusUser = New System.Windows.Forms.ToolStripStatusLabel()
		Me.StatusProgress = New System.Windows.Forms.ToolStripProgressBar()
		Me.BWupdate = New System.ComponentModel.BackgroundWorker()
		Me.NotifyIcon = New System.Windows.Forms.NotifyIcon(Me.components)
		Me.TabAbout = New System.Windows.Forms.TabPage()
		Me.Label7 = New System.Windows.Forms.Label()
		Me.TextBoxAbout = New System.Windows.Forms.TextBox()
		Me.Tabs.SuspendLayout()
		Me.TabNews.SuspendLayout()
		Me.TabManageMods.SuspendLayout()
		Me.ContextMenuList.SuspendLayout()
		Me.TabSettings.SuspendLayout()
		Me.StatusStrip.SuspendLayout()
		Me.TabAbout.SuspendLayout()
		Me.SuspendLayout()
		'
		'modpack
		'
		Me.modpack.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
			Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.modpack.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.modpack.FormattingEnabled = True
		Me.modpack.Location = New System.Drawing.Point(92, 616)
		Me.modpack.Name = "modpack"
		Me.modpack.Size = New System.Drawing.Size(308, 21)
		Me.modpack.Sorted = True
		Me.modpack.TabIndex = 3
		Me.ToolTip.SetToolTip(Me.modpack, "Select the modpack to launch Custom Throne with.")
		'
		'EarlyAccess
		'
		Me.EarlyAccess.AutoSize = True
		Me.EarlyAccess.Location = New System.Drawing.Point(12, 38)
		Me.EarlyAccess.Name = "EarlyAccess"
		Me.EarlyAccess.Size = New System.Drawing.Size(238, 17)
		Me.EarlyAccess.TabIndex = 11
		Me.EarlyAccess.Text = "Use Early Access versions of Custom Throne"
		Me.ToolTip.SetToolTip(Me.EarlyAccess, "In addition to stable versions of Custom Throne, there are more regularly updated" &
		" Early Access versions. Be aware that they are less tested and more buggy as a r" &
		"esult.")
		Me.EarlyAccess.UseVisualStyleBackColor = True
		'
		'NTFolder
		'
		Me.NTFolder.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
			Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.NTFolder.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
		Me.NTFolder.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.FileSystemDirectories
		Me.NTFolder.Location = New System.Drawing.Point(124, 8)
		Me.NTFolder.Name = "NTFolder"
		Me.NTFolder.Size = New System.Drawing.Size(488, 20)
		Me.NTFolder.TabIndex = 8
		Me.ToolTip.SetToolTip(Me.NTFolder, "The folder where Nuclear Throne's executable and game data lies, as well as Custo" &
		"m Throne.")
		'
		'UpdateLaunch
		'
		Me.UpdateLaunch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.UpdateLaunch.FormattingEnabled = True
		Me.UpdateLaunch.Items.AddRange(New Object() {"Do nothing", "Check for updates, notify if any", "Install updates, if any"})
		Me.UpdateLaunch.Location = New System.Drawing.Point(200, 60)
		Me.UpdateLaunch.Name = "UpdateLaunch"
		Me.UpdateLaunch.Size = New System.Drawing.Size(184, 21)
		Me.UpdateLaunch.TabIndex = 12
		Me.ToolTip.SetToolTip(Me.UpdateLaunch, "How updates are handled on launch of Custom Throne")
		'
		'LaunchSettings
		'
		Me.LaunchSettings.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.LaunchSettings.FormattingEnabled = True
		Me.LaunchSettings.Items.AddRange(New Object() {"Nothing", "Minimize Launcher (Taskbar)", "Minimize Launcher (Tray)", "Close Launcher"})
		Me.LaunchSettings.Location = New System.Drawing.Point(200, 88)
		Me.LaunchSettings.Name = "LaunchSettings"
		Me.LaunchSettings.Size = New System.Drawing.Size(184, 21)
		Me.LaunchSettings.TabIndex = 15
		Me.ToolTip.SetToolTip(Me.LaunchSettings, "What should be done when Custom Throne is launched through the launcher.")
		'
		'SteamInGame
		'
		Me.SteamInGame.AutoSize = True
		Me.SteamInGame.Location = New System.Drawing.Point(12, 140)
		Me.SteamInGame.Name = "SteamInGame"
		Me.SteamInGame.Size = New System.Drawing.Size(247, 17)
		Me.SteamInGame.TabIndex = 16
		Me.SteamInGame.Text = "Show as ""In-Game"" while running the launcher"
		Me.ToolTip.SetToolTip(Me.SteamInGame, "While the launcher is open, Steam will show you as ""In-Game : Nuclear Throne"".")
		Me.SteamInGame.UseVisualStyleBackColor = True
		Me.SteamInGame.Visible = False
		'
		'ButtonLaunch
		'
		Me.ButtonLaunch.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.ButtonLaunch.DialogResult = System.Windows.Forms.DialogResult.OK
		Me.ButtonLaunch.ForeColor = System.Drawing.SystemColors.WindowFrame
		Me.ButtonLaunch.Image = Global.CTLauncher.My.Resources.Resources.application_go
		Me.ButtonLaunch.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.ButtonLaunch.Location = New System.Drawing.Point(544, 608)
		Me.ButtonLaunch.Name = "ButtonLaunch"
		Me.ButtonLaunch.Size = New System.Drawing.Size(128, 35)
		Me.ButtonLaunch.TabIndex = 0
		Me.ButtonLaunch.Text = "Launch!"
		Me.ButtonLaunch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
		Me.ToolTip.SetToolTip(Me.ButtonLaunch, "Launches Custom Throne with the selected mod pack.")
		Me.ButtonLaunch.UseVisualStyleBackColor = True
		'
		'ButtonUpdate
		'
		Me.ButtonUpdate.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.ButtonUpdate.ForeColor = System.Drawing.SystemColors.WindowFrame
		Me.ButtonUpdate.Image = Global.CTLauncher.My.Resources.Resources.arrow_refresh
		Me.ButtonUpdate.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.ButtonUpdate.Location = New System.Drawing.Point(404, 608)
		Me.ButtonUpdate.Name = "ButtonUpdate"
		Me.ButtonUpdate.Size = New System.Drawing.Size(139, 35)
		Me.ButtonUpdate.TabIndex = 8
		Me.ButtonUpdate.Text = "Check for Updates"
		Me.ButtonUpdate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
		Me.ToolTip.SetToolTip(Me.ButtonUpdate, "Check for updates")
		Me.ButtonUpdate.UseVisualStyleBackColor = True
		'
		'ButtonImportMP
		'
		Me.ButtonImportMP.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.ButtonImportMP.ForeColor = System.Drawing.SystemColors.WindowFrame
		Me.ButtonImportMP.Image = Global.CTLauncher.My.Resources.Resources.folder_page
		Me.ButtonImportMP.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.ButtonImportMP.Location = New System.Drawing.Point(256, 548)
		Me.ButtonImportMP.Name = "ButtonImportMP"
		Me.ButtonImportMP.Size = New System.Drawing.Size(108, 24)
		Me.ButtonImportMP.TabIndex = 4
		Me.ButtonImportMP.Text = "Import..."
		Me.ButtonImportMP.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
		Me.ToolTip.SetToolTip(Me.ButtonImportMP, "Imports an existing modpack")
		Me.ButtonImportMP.UseVisualStyleBackColor = True
		'
		'ButtonRemoveMPs
		'
		Me.ButtonRemoveMPs.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.ButtonRemoveMPs.ForeColor = System.Drawing.SystemColors.WindowFrame
		Me.ButtonRemoveMPs.Image = Global.CTLauncher.My.Resources.Resources.delete
		Me.ButtonRemoveMPs.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.ButtonRemoveMPs.Location = New System.Drawing.Point(124, 548)
		Me.ButtonRemoveMPs.Name = "ButtonRemoveMPs"
		Me.ButtonRemoveMPs.Size = New System.Drawing.Size(124, 24)
		Me.ButtonRemoveMPs.TabIndex = 3
		Me.ButtonRemoveMPs.Text = "Remove Selected"
		Me.ButtonRemoveMPs.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
		Me.ToolTip.SetToolTip(Me.ButtonRemoveMPs, "Removes the selected modpacks")
		Me.ButtonRemoveMPs.UseVisualStyleBackColor = True
		'
		'ButtonCreateMP
		'
		Me.ButtonCreateMP.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.ButtonCreateMP.ForeColor = System.Drawing.SystemColors.WindowFrame
		Me.ButtonCreateMP.Image = Global.CTLauncher.My.Resources.Resources.add
		Me.ButtonCreateMP.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.ButtonCreateMP.Location = New System.Drawing.Point(8, 548)
		Me.ButtonCreateMP.Name = "ButtonCreateMP"
		Me.ButtonCreateMP.Size = New System.Drawing.Size(108, 24)
		Me.ButtonCreateMP.TabIndex = 2
		Me.ButtonCreateMP.Text = "Create New"
		Me.ButtonCreateMP.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
		Me.ToolTip.SetToolTip(Me.ButtonCreateMP, "Creates a new modpack")
		Me.ButtonCreateMP.UseVisualStyleBackColor = True
		'
		'ButtonSaveSettings
		'
		Me.ButtonSaveSettings.ForeColor = System.Drawing.SystemColors.WindowFrame
		Me.ButtonSaveSettings.Image = Global.CTLauncher.My.Resources.Resources.accept
		Me.ButtonSaveSettings.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.ButtonSaveSettings.Location = New System.Drawing.Point(16, 164)
		Me.ButtonSaveSettings.Name = "ButtonSaveSettings"
		Me.ButtonSaveSettings.Size = New System.Drawing.Size(107, 23)
		Me.ButtonSaveSettings.TabIndex = 17
		Me.ButtonSaveSettings.Text = "Save Settings"
		Me.ButtonSaveSettings.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
		Me.ToolTip.SetToolTip(Me.ButtonSaveSettings, "Saves the settings (will get auto-saved on launch or when closing)")
		Me.ButtonSaveSettings.UseVisualStyleBackColor = True
		'
		'ButtonDetect
		'
		Me.ButtonDetect.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.ButtonDetect.Image = Global.CTLauncher.My.Resources.Resources.magnifier
		Me.ButtonDetect.Location = New System.Drawing.Point(616, 8)
		Me.ButtonDetect.Name = "ButtonDetect"
		Me.ButtonDetect.Size = New System.Drawing.Size(23, 23)
		Me.ButtonDetect.TabIndex = 10
		Me.ToolTip.SetToolTip(Me.ButtonDetect, "Let the launcher auto-detect the location of Nuclear Throne.")
		Me.ButtonDetect.UseVisualStyleBackColor = True
		'
		'ButtonBrowse
		'
		Me.ButtonBrowse.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.ButtonBrowse.Image = Global.CTLauncher.My.Resources.Resources.folder_go
		Me.ButtonBrowse.Location = New System.Drawing.Point(640, 8)
		Me.ButtonBrowse.Name = "ButtonBrowse"
		Me.ButtonBrowse.Size = New System.Drawing.Size(23, 23)
		Me.ButtonBrowse.TabIndex = 9
		Me.ToolTip.SetToolTip(Me.ButtonBrowse, "Browse for Nuclear Throne's folder.")
		Me.ButtonBrowse.UseVisualStyleBackColor = True
		'
		'UpdateCheckInterrupt
		'
		Me.UpdateCheckInterrupt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.UpdateCheckInterrupt.FormattingEnabled = True
		Me.UpdateCheckInterrupt.Items.AddRange(New Object() {"Prompt User", "Cancel Update Check", "Wait until Finished"})
		Me.UpdateCheckInterrupt.Location = New System.Drawing.Point(200, 116)
		Me.UpdateCheckInterrupt.Name = "UpdateCheckInterrupt"
		Me.UpdateCheckInterrupt.Size = New System.Drawing.Size(184, 21)
		Me.UpdateCheckInterrupt.TabIndex = 19
		Me.ToolTip.SetToolTip(Me.UpdateCheckInterrupt, "What should be done if Custom Throne is launched while checking for updates.")
		'
		'ButtonReloadMPs
		'
		Me.ButtonReloadMPs.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.ButtonReloadMPs.ForeColor = System.Drawing.SystemColors.WindowFrame
		Me.ButtonReloadMPs.Image = Global.CTLauncher.My.Resources.Resources.arrow_refresh
		Me.ButtonReloadMPs.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.ButtonReloadMPs.Location = New System.Drawing.Point(368, 548)
		Me.ButtonReloadMPs.Name = "ButtonReloadMPs"
		Me.ButtonReloadMPs.Size = New System.Drawing.Size(112, 24)
		Me.ButtonReloadMPs.TabIndex = 5
		Me.ButtonReloadMPs.Text = "Reload"
		Me.ButtonReloadMPs.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
		Me.ToolTip.SetToolTip(Me.ButtonReloadMPs, "Reloads the list of modpacks")
		Me.ButtonReloadMPs.UseVisualStyleBackColor = True
		'
		'WebBrowser
		'
		Me.WebBrowser.AllowNavigation = False
		Me.WebBrowser.AllowWebBrowserDrop = False
		Me.WebBrowser.Dock = System.Windows.Forms.DockStyle.Fill
		Me.WebBrowser.IsWebBrowserContextMenuEnabled = False
		Me.WebBrowser.Location = New System.Drawing.Point(3, 3)
		Me.WebBrowser.MinimumSize = New System.Drawing.Size(20, 20)
		Me.WebBrowser.Name = "WebBrowser"
		Me.WebBrowser.ScriptErrorsSuppressed = True
		Me.WebBrowser.Size = New System.Drawing.Size(662, 572)
		Me.WebBrowser.TabIndex = 1
		Me.WebBrowser.Url = New System.Uri("http://customthrone.tumblr.com/", System.UriKind.Absolute)
		Me.WebBrowser.WebBrowserShortcutsEnabled = False
		'
		'Label1
		'
		Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.Label1.AutoSize = True
		Me.Label1.Location = New System.Drawing.Point(4, 620)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(88, 13)
		Me.Label1.TabIndex = 4
		Me.Label1.Text = "Active Modpack:"
		'
		'Tabs
		'
		Me.Tabs.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
			Or System.Windows.Forms.AnchorStyles.Left) _
			Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Tabs.Controls.Add(Me.TabNews)
		Me.Tabs.Controls.Add(Me.TabManageMods)
		Me.Tabs.Controls.Add(Me.TabSettings)
		Me.Tabs.Controls.Add(Me.TabAbout)
		Me.Tabs.Location = New System.Drawing.Point(0, 4)
		Me.Tabs.Name = "Tabs"
		Me.Tabs.SelectedIndex = 0
		Me.Tabs.Size = New System.Drawing.Size(676, 604)
		Me.Tabs.TabIndex = 6
		'
		'TabNews
		'
		Me.TabNews.BackColor = System.Drawing.SystemColors.Window
		Me.TabNews.Controls.Add(Me.WebBrowser)
		Me.TabNews.Location = New System.Drawing.Point(4, 22)
		Me.TabNews.Name = "TabNews"
		Me.TabNews.Padding = New System.Windows.Forms.Padding(3)
		Me.TabNews.Size = New System.Drawing.Size(668, 578)
		Me.TabNews.TabIndex = 0
		Me.TabNews.Text = "News"
		'
		'TabManageMods
		'
		Me.TabManageMods.BackColor = System.Drawing.SystemColors.Window
		Me.TabManageMods.Controls.Add(Me.ButtonReloadMPs)
		Me.TabManageMods.Controls.Add(Me.ButtonImportMP)
		Me.TabManageMods.Controls.Add(Me.ButtonRemoveMPs)
		Me.TabManageMods.Controls.Add(Me.ButtonCreateMP)
		Me.TabManageMods.Controls.Add(Me.ListModPacks)
		Me.TabManageMods.Controls.Add(Me.Label3)
		Me.TabManageMods.Location = New System.Drawing.Point(4, 22)
		Me.TabManageMods.Name = "TabManageMods"
		Me.TabManageMods.Padding = New System.Windows.Forms.Padding(3)
		Me.TabManageMods.Size = New System.Drawing.Size(668, 578)
		Me.TabManageMods.TabIndex = 2
		Me.TabManageMods.Text = "Manage Mods"
		'
		'ListModPacks
		'
		Me.ListModPacks.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
			Or System.Windows.Forms.AnchorStyles.Left) _
			Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.ListModPacks.BackColor = System.Drawing.SystemColors.ControlDark
		Me.ListModPacks.CheckBoxes = True
		Me.ListModPacks.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnActive, Me.ColumnName, Me.ColumnPath, Me.ColumnAuthor})
		Me.ListModPacks.ContextMenuStrip = Me.ContextMenuList
		Me.ListModPacks.FullRowSelect = True
		Me.ListModPacks.GridLines = True
		Me.ListModPacks.HideSelection = False
		Me.ListModPacks.Location = New System.Drawing.Point(8, 28)
		Me.ListModPacks.Name = "ListModPacks"
		Me.ListModPacks.Size = New System.Drawing.Size(652, 516)
		Me.ListModPacks.Sorting = System.Windows.Forms.SortOrder.Ascending
		Me.ListModPacks.TabIndex = 1
		Me.ListModPacks.UseCompatibleStateImageBehavior = False
		Me.ListModPacks.View = System.Windows.Forms.View.Details
		'
		'ColumnActive
		'
		Me.ColumnActive.Text = "Active"
		Me.ColumnActive.Width = 45
		'
		'ColumnName
		'
		Me.ColumnName.Text = "Name"
		Me.ColumnName.Width = 175
		'
		'ColumnPath
		'
		Me.ColumnPath.Text = "Path"
		Me.ColumnPath.Width = 215
		'
		'ColumnAuthor
		'
		Me.ColumnAuthor.Text = "Author"
		Me.ColumnAuthor.Width = 175
		'
		'ContextMenuList
		'
		Me.ContextMenuList.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CreateNewToolStripMenuItem, Me.RemoveSelectedToolStripMenuItem, Me.ImportToolStripMenuItem, Me.ReloadToolStripMenuItem})
		Me.ContextMenuList.Name = "ContextMenuList"
		Me.ContextMenuList.Size = New System.Drawing.Size(158, 92)
		'
		'CreateNewToolStripMenuItem
		'
		Me.CreateNewToolStripMenuItem.Image = Global.CTLauncher.My.Resources.Resources.add
		Me.CreateNewToolStripMenuItem.Name = "CreateNewToolStripMenuItem"
		Me.CreateNewToolStripMenuItem.Size = New System.Drawing.Size(157, 22)
		Me.CreateNewToolStripMenuItem.Text = "Create New"
		'
		'RemoveSelectedToolStripMenuItem
		'
		Me.RemoveSelectedToolStripMenuItem.Image = Global.CTLauncher.My.Resources.Resources.delete
		Me.RemoveSelectedToolStripMenuItem.Name = "RemoveSelectedToolStripMenuItem"
		Me.RemoveSelectedToolStripMenuItem.Size = New System.Drawing.Size(157, 22)
		Me.RemoveSelectedToolStripMenuItem.Text = "Remove Selected"
		'
		'ImportToolStripMenuItem
		'
		Me.ImportToolStripMenuItem.Image = Global.CTLauncher.My.Resources.Resources.folder_page
		Me.ImportToolStripMenuItem.Name = "ImportToolStripMenuItem"
		Me.ImportToolStripMenuItem.Size = New System.Drawing.Size(157, 22)
		Me.ImportToolStripMenuItem.Text = "Import..."
		'
		'ReloadToolStripMenuItem
		'
		Me.ReloadToolStripMenuItem.Image = Global.CTLauncher.My.Resources.Resources.arrow_refresh
		Me.ReloadToolStripMenuItem.Name = "ReloadToolStripMenuItem"
		Me.ReloadToolStripMenuItem.Size = New System.Drawing.Size(157, 22)
		Me.ReloadToolStripMenuItem.Text = "Reload"
		'
		'Label3
		'
		Me.Label3.AutoSize = True
		Me.Label3.Location = New System.Drawing.Point(8, 8)
		Me.Label3.Name = "Label3"
		Me.Label3.Size = New System.Drawing.Size(91, 13)
		Me.Label3.TabIndex = 0
		Me.Label3.Text = "List of Modpacks:"
		'
		'TabSettings
		'
		Me.TabSettings.BackColor = System.Drawing.SystemColors.Window
		Me.TabSettings.Controls.Add(Me.UpdateCheckInterrupt)
		Me.TabSettings.Controls.Add(Me.Label6)
		Me.TabSettings.Controls.Add(Me.ButtonSaveSettings)
		Me.TabSettings.Controls.Add(Me.SteamInGame)
		Me.TabSettings.Controls.Add(Me.LaunchSettings)
		Me.TabSettings.Controls.Add(Me.Label5)
		Me.TabSettings.Controls.Add(Me.Label4)
		Me.TabSettings.Controls.Add(Me.UpdateLaunch)
		Me.TabSettings.Controls.Add(Me.EarlyAccess)
		Me.TabSettings.Controls.Add(Me.NTFolder)
		Me.TabSettings.Controls.Add(Me.Label2)
		Me.TabSettings.Controls.Add(Me.ButtonDetect)
		Me.TabSettings.Controls.Add(Me.ButtonBrowse)
		Me.TabSettings.Location = New System.Drawing.Point(4, 22)
		Me.TabSettings.Name = "TabSettings"
		Me.TabSettings.Padding = New System.Windows.Forms.Padding(3)
		Me.TabSettings.Size = New System.Drawing.Size(668, 578)
		Me.TabSettings.TabIndex = 1
		Me.TabSettings.Text = "Settings"
		'
		'Label6
		'
		Me.Label6.AutoSize = True
		Me.Label6.Location = New System.Drawing.Point(12, 116)
		Me.Label6.Name = "Label6"
		Me.Label6.Size = New System.Drawing.Size(149, 13)
		Me.Label6.TabIndex = 18
		Me.Label6.Text = "... while checking for updates:"
		'
		'Label5
		'
		Me.Label5.AutoSize = True
		Me.Label5.Location = New System.Drawing.Point(12, 92)
		Me.Label5.Name = "Label5"
		Me.Label5.Size = New System.Drawing.Size(148, 13)
		Me.Label5.TabIndex = 14
		Me.Label5.Text = "On launching Custom Throne:"
		'
		'Label4
		'
		Me.Label4.AutoSize = True
		Me.Label4.Location = New System.Drawing.Point(12, 64)
		Me.Label4.Name = "Label4"
		Me.Label4.Size = New System.Drawing.Size(184, 13)
		Me.Label4.TabIndex = 13
		Me.Label4.Text = "On starting Custom Throne Launcher:"
		'
		'Label2
		'
		Me.Label2.AutoSize = True
		Me.Label2.Location = New System.Drawing.Point(8, 12)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(116, 13)
		Me.Label2.TabIndex = 7
		Me.Label2.Text = "Nuclear Throne Folder:"
		'
		'StatusStrip
		'
		Me.StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.StatusText, Me.StatusSpring, Me.StatusUser, Me.StatusProgress})
		Me.StatusStrip.Location = New System.Drawing.Point(0, 646)
		Me.StatusStrip.Name = "StatusStrip"
		Me.StatusStrip.Size = New System.Drawing.Size(677, 22)
		Me.StatusStrip.TabIndex = 7
		Me.StatusStrip.Text = "StatusStrip"
		'
		'StatusText
		'
		Me.StatusText.Name = "StatusText"
		Me.StatusText.Size = New System.Drawing.Size(38, 17)
		Me.StatusText.Text = "Ready"
		'
		'StatusSpring
		'
		Me.StatusSpring.Name = "StatusSpring"
		Me.StatusSpring.Size = New System.Drawing.Size(479, 17)
		Me.StatusSpring.Spring = True
		'
		'StatusUser
		'
		Me.StatusUser.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Left
		Me.StatusUser.BorderStyle = System.Windows.Forms.Border3DStyle.Bump
		Me.StatusUser.Name = "StatusUser"
		Me.StatusUser.Size = New System.Drawing.Size(43, 17)
		Me.StatusUser.Text = "Offline"
		'
		'StatusProgress
		'
		Me.StatusProgress.MarqueeAnimationSpeed = 50
		Me.StatusProgress.Name = "StatusProgress"
		Me.StatusProgress.Size = New System.Drawing.Size(100, 16)
		'
		'BWupdate
		'
		Me.BWupdate.WorkerSupportsCancellation = True
		'
		'NotifyIcon
		'
		Me.NotifyIcon.Icon = CType(resources.GetObject("NotifyIcon.Icon"), System.Drawing.Icon)
		Me.NotifyIcon.Text = "Custom Throne Launcher"
		'
		'TabAbout
		'
		Me.TabAbout.BackColor = System.Drawing.SystemColors.Window
		Me.TabAbout.Controls.Add(Me.TextBoxAbout)
		Me.TabAbout.Controls.Add(Me.Label7)
		Me.TabAbout.Location = New System.Drawing.Point(4, 22)
		Me.TabAbout.Name = "TabAbout"
		Me.TabAbout.Padding = New System.Windows.Forms.Padding(3)
		Me.TabAbout.Size = New System.Drawing.Size(668, 578)
		Me.TabAbout.TabIndex = 3
		Me.TabAbout.Text = "About"
		'
		'Label7
		'
		Me.Label7.AutoSize = True
		Me.Label7.Location = New System.Drawing.Point(8, 8)
		Me.Label7.Name = "Label7"
		Me.Label7.Size = New System.Drawing.Size(161, 13)
		Me.Label7.TabIndex = 0
		Me.Label7.Text = "About Custom Throne Launcher:"
		'
		'TextBoxAbout
		'
		Me.TextBoxAbout.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
			Or System.Windows.Forms.AnchorStyles.Left) _
			Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.TextBoxAbout.Location = New System.Drawing.Point(8, 28)
		Me.TextBoxAbout.Multiline = True
		Me.TextBoxAbout.Name = "TextBoxAbout"
		Me.TextBoxAbout.ReadOnly = True
		Me.TextBoxAbout.Size = New System.Drawing.Size(652, 540)
		Me.TextBoxAbout.TabIndex = 1
		Me.TextBoxAbout.Text = resources.GetString("TextBoxAbout.Text")
		'
		'Main
		'
		Me.AcceptButton = Me.ButtonLaunch
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(677, 668)
		Me.Controls.Add(Me.ButtonUpdate)
		Me.Controls.Add(Me.StatusStrip)
		Me.Controls.Add(Me.Tabs)
		Me.Controls.Add(Me.Label1)
		Me.Controls.Add(Me.modpack)
		Me.Controls.Add(Me.ButtonLaunch)
		Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
		Me.MinimumSize = New System.Drawing.Size(510, 400)
		Me.Name = "Main"
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
		Me.Text = "Custom Throne Launcher"
		Me.Tabs.ResumeLayout(False)
		Me.TabNews.ResumeLayout(False)
		Me.TabManageMods.ResumeLayout(False)
		Me.TabManageMods.PerformLayout()
		Me.ContextMenuList.ResumeLayout(False)
		Me.TabSettings.ResumeLayout(False)
		Me.TabSettings.PerformLayout()
		Me.StatusStrip.ResumeLayout(False)
		Me.StatusStrip.PerformLayout()
		Me.TabAbout.ResumeLayout(False)
		Me.TabAbout.PerformLayout()
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub

	Friend WithEvents ButtonLaunch As Button
	Friend WithEvents ToolTip As ToolTip
	Friend WithEvents WebBrowser As WebBrowser
	Friend WithEvents modpack As ComboBox
	Friend WithEvents Label1 As Label
	Friend WithEvents Tabs As TabControl
	Friend WithEvents TabNews As TabPage
	Friend WithEvents TabSettings As TabPage
	Friend WithEvents EarlyAccess As CheckBox
	Friend WithEvents ButtonDetect As Button
	Friend WithEvents ButtonBrowse As Button
	Friend WithEvents NTFolder As TextBox
	Friend WithEvents Label2 As Label
	Friend WithEvents TabManageMods As TabPage
	Friend WithEvents ListModPacks As ListView
	Friend WithEvents ColumnName As ColumnHeader
	Friend WithEvents ColumnPath As ColumnHeader
	Friend WithEvents Label3 As Label
	Friend WithEvents ButtonCreateMP As Button
	Friend WithEvents ButtonRemoveMPs As Button
	Friend WithEvents ButtonImportMP As Button
	Friend WithEvents Label4 As Label
	Friend WithEvents UpdateLaunch As ComboBox
	Friend WithEvents StatusStrip As StatusStrip
	Friend WithEvents StatusText As ToolStripStatusLabel
	Friend WithEvents StatusSpring As ToolStripStatusLabel
	Friend WithEvents StatusProgress As ToolStripProgressBar
	Friend WithEvents ButtonUpdate As Button
	Friend WithEvents BWupdate As System.ComponentModel.BackgroundWorker
	Friend WithEvents ColumnAuthor As ColumnHeader
	Friend WithEvents StatusUser As ToolStripStatusLabel
	Friend WithEvents LaunchSettings As ComboBox
	Friend WithEvents Label5 As Label
	Friend WithEvents NotifyIcon As NotifyIcon
	Friend WithEvents SteamInGame As CheckBox
	Friend WithEvents ButtonSaveSettings As Button
	Friend WithEvents UpdateCheckInterrupt As ComboBox
	Friend WithEvents Label6 As Label
	Friend WithEvents ButtonReloadMPs As Button
	Friend WithEvents ColumnActive As ColumnHeader
	Friend WithEvents ContextMenuList As ContextMenuStrip
	Friend WithEvents CreateNewToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents RemoveSelectedToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents ImportToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents ReloadToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents TabAbout As TabPage
	Friend WithEvents TextBoxAbout As TextBox
	Friend WithEvents Label7 As Label
End Class
