﻿Imports System.ComponentModel
Imports System.IO
Imports System.IO.Compression
Imports System.Threading
Imports Steamworks

Public Class Main
	Private versionURL As String = "https://www.dropbox.com/s/3kx3t5q0275baic/version2.txt?dl=1"
	Private dirlist As String() = {"areas", "characters", "config", "enemies", "levels", "mutations", "ultras", "projectiles", "weapons"}
	Private username As String = Nothing
	Private dlloc As String
	Private updatechecking As Boolean = False
	Private launchwhendone As Boolean = False
	Private dlclient As TimedWebClient

	Sub New()
        'Set this to work around some localization problems:
        '- Partial usage of commas instead of periods
        '- Error messages are in local language
        Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-US")
		Thread.CurrentThread.CurrentUICulture = New Globalization.CultureInfo("en-US")

		InitializeComponent()
	End Sub

	Private Sub Main_Load(sender As Object, e As EventArgs) Handles MyBase.Load
		Try
			'Set title of window
			Me.Text = "Custom Throne Launcher v" & Application.ProductVersion.Substring(0, Application.ProductVersion.Length - 2)

			If My.Settings.firstlaunch Then
				'Run first launch-specific stuff
				My.Settings.firstlaunch = False

				'Detect NT folder
				Dim foundfolder = DetectNTFolder()
				If foundfolder Is Nothing Then
					'Did not detect NT folder
					If MessageBox.Show("Custom Throne Launcher was not able to detect your installation of Nuclear Throne." & vbCrLf & vbCrLf &
								   "Would you like to manually locate it now?", "Could not detect NT folder",
								   MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
						Tabs.SelectTab("TabSettings")
					End If
				Else
					My.Settings.ntfolder = foundfolder
				End If
			Else
				'Load last window state only if not first launch!
				Me.WindowState = My.Settings.windowstate
				Me.Location = My.Settings.windowpos
				Me.Size = My.Settings.windowsize
			End If

			'Start async auto-update check (only if valid NT folder is present) - notify or install
			If My.Settings.UpdateLaunch > 0 AndAlso DirManager.ValidNTFolder() Then
				UpdateCheck(My.Settings.UpdateLaunch = 2)
			End If

			'Initialize SteamAPI for username and userID
			InitSteamAPI()

			'Load any saved settings (for TabSettings)
			LoadSettings()

			'Check if Custom Throne is installed
			If DirManager.ValidNTFolder() AndAlso File.Exists(DirManager.CustomThroneExecutable) Then
				'Allow launching
				ButtonLaunch.Enabled = True
			End If

			'Load modpack list
			ReloadModPacks()

			'Select last active modpack
			If My.Settings.modpack = "default" Then
				'Select default modpack
				modpack.SelectedItem = GetModPack("default")
			Else
				'Select found modpack
				Try
					modpack.SelectedItem = GetModPack(My.Settings.modpack)
				Catch
					'TODO: Mod has not been found, warn user?
					modpack.SelectedItem = GetModPack("default")
				End Try
			End If

			'Select modpack in listview as well
			ListModPacks.Items.Item(DirectCast(modpack.SelectedItem, ModPack).InternalName).Checked = True

			'Create folders for active modpack
			CreateModFolders(modpack.SelectedItem)
		Catch ex As Exception
			ShowErrorMessage(ex, "An error occured while loading the launcher!")
		End Try
	End Sub

	Private Sub Main_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
		Try
			'Save settings
			ApplySettings()

			'Cancel the update check!
			CancelUpdateCheck()

			'Save window state
			My.Settings.windowstate = Me.WindowState
			My.Settings.windowpos = Me.Location
			My.Settings.windowsize = Me.Size
		Catch ex As Exception
			ShowErrorMessage(ex, "An error occured while closing the launcher!")
		End Try
	End Sub

	Private Sub ButtonLaunch_Click() Handles ButtonLaunch.Click
		Try
			'Show user input!
			ButtonLaunch.Enabled = False

			'Wait for auto-update check to finish / allow user to skip it
			If updatechecking Then
				'Decide action
				Select Case My.Settings.UpdateCheckInterrupt
					Case 0
						If LaunchPending.ShowDialog() = DialogResult.Cancel Then
							'Cancel the update check!
							CancelUpdateCheck()
						Else
							'Wait for update to finish (in background).
							launchwhendone = True
							ButtonLaunch.Enabled = True
							Return
						End If
					Case 1
						'Cancel update check
						CancelUpdateCheck()
					Case 2
						'Wait for it to finish
						launchwhendone = True
						ButtonLaunch.Enabled = False
						Return
				End Select
			End If

			'Reset
			launchwhendone = False

			'Verify valid NT install
			If Not DirManager.ValidNTFolder() Then
				MessageBox.Show("No valid location for Nuclear Throne was specified. Because of this, Custom Throne can not be launched.",
								"No NT folder specified", MessageBoxButtons.OK, MessageBoxIcon.Information)
				Tabs.SelectTab("TabSettings")
				Return
			End If

			'Save settings
			ApplySettings()

			'Get current modpack name
			Dim activemodpack As ModPack = modpack.SelectedItem

			'Create folders
			CreateModFolders(activemodpack)

			'Attached userinfo
			Dim userinfo As String = ""
			If username IsNot Nothing AndAlso username <> "" Then
				'TODO: Make username friendly for the font (-> some special chars do not exist in that font, not sure how unicode would be handled either)
				userinfo = " """ & username & """"
			End If

			'Launch Custom Throne
			If File.Exists(DirManager.CustomThroneExecutable) Then
				Process.Start(DirManager.CustomThroneExecutable, """" & activemodpack.InternalName & """" & userinfo)

				'Decide action
				Select Case My.Settings.LaunchSettings
					Case 1
						'Minimize (taskbar)
						Me.WindowState = FormWindowState.Minimized
					Case 2
						'Minimize (tray)
						Me.Hide()
						NotifyIcon.Visible = True
					Case 3
						'Close launcher
						Me.Close()
				End Select
			Else
				'Can't find CT!
				MessageBox.Show("Custom Throne can not be found! Please let the launcher run an update to download the latest version.", "Custom Throne not found",
								MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
			End If

		Catch ex As Exception
			ShowErrorMessage(ex, "An error occured while launching Custom Throne!")
		End Try

		'Reset UI
		ButtonLaunch.Enabled = True
	End Sub

	Private Sub ButtonUpdate_Click(sender As Object, e As EventArgs) Handles ButtonUpdate.Click
		'Check for updates
		UpdateCheck(False)
	End Sub

	Private Sub ButtonCreateMP_Click(sender As Object, e As EventArgs) Handles ButtonCreateMP.Click, CreateNewToolStripMenuItem.Click
		'Save settings beforehand
		ApplySettings()

		Try

			'Create a new modpack
			If NewMPaction.ShowDialog() = DialogResult.OK Then
				Dim sourcemp As ModPack = NewMPaction.ModSource.SelectedItem
				Dim dispname = NewMPaction.ModName.Text

				'Set name only if non-empty
				If NewMPaction.ModDisplay.Text <> "" Then
					dispname = NewMPaction.ModDisplay.Text
				End If

				'Creates the modpack
				Dim mp As New ModPack(NewMPaction.ModName.Text, dispname)
				If sourcemp.InternalName = "default" Then
					'Create basic folders
					CreateModFolders(mp)
				Else
					'Copy existing modpack folders
					CreateModFolders(sourcemp)
					My.Computer.FileSystem.CopyDirectory(sourcemp.FullPath, mp.FullPath)
				End If

				'(Re)create config file
				Dim cfgpath = Path.Combine(mp.FullPath, "config\config.ini")
				File.Delete(cfgpath)

				Dim cfgfile = File.CreateText(cfgpath)
				cfgfile.WriteLine("[config]")
				cfgfile.WriteLine("name=" & mp.DisplayName)
				cfgfile.WriteLine("author=" & NewMPaction.ModAuthor.Text)
				cfgfile.Close()

				'Reload mod list
				ReloadModPacks()
			End If

		Catch ex As Exception
			ShowErrorMessage(ex, "An error occured creating a new modpack!")
		End Try
	End Sub

	Private Sub ButtonRemoveMPs_Click(sender As Object, e As EventArgs) Handles ButtonRemoveMPs.Click, RemoveSelectedToolStripMenuItem.Click
		'Save settings beforehand
		ApplySettings()

		Try
			Dim modlist As New List(Of ModPack)

			If ListModPacks.SelectedItems.Count = 0 Then
				'Use checkboxed
				Dim checked = ListModPacks.CheckedItems.Item(0).Name
				If checked <> "default" Then
					modlist.Add(GetModPack(checked))
				End If
			Else
				For Each sel As ListViewItem In ListModPacks.SelectedItems
					If sel.Text <> "default" Then
						modlist.Add(GetModPack(sel.Name))
					End If
				Next
			End If

			'Remove selected mods (what a nice piece of code)
			If MessageBox.Show("Are you sure you would like to remove the following " & Utils.UseS(modlist.Count, "mod", UseSOptions.NoNumberForOne) &
							   " completely, including their directory and data?" & vbCrLf & vbCrLf & "List of mods:" & vbCrLf &
							   String.Join(vbCrLf, modlist), "Remove " & Utils.UseS(modlist.Count, "mod"), MessageBoxButtons.YesNo,
							   MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = DialogResult.Yes Then
				For Each mp In modlist
					'Remove the folder and files
					Directory.Delete(mp.FullPath, True)
				Next

				'Reload lists
				ReloadModPacks()
			End If

		Catch ex As Exception
			ShowErrorMessage(ex, "An error occured removing the selected mods!")
		End Try
	End Sub

	Private Sub ButtonImportMP_Click(sender As Object, e As EventArgs) Handles ButtonImportMP.Click, ImportToolStripMenuItem.Click
		'Save settings beforehand
		ApplySettings()

		Using ofd As New OpenFileDialog
			ofd.AddExtension = True
			ofd.CheckFileExists = True
			ofd.CheckPathExists = True
			ofd.InitialDirectory = DirManager.NuclearThroneDirectory
			ofd.Filter = "ZIP Archives|*.zip"
			ofd.Title = "Import Modpack"
			If ofd.ShowDialog = DialogResult.OK Then
				'Import modpack
				Dim extractdir = Path.Combine(DirManager.TempDirectory, "extracted")
				Directory.CreateDirectory(extractdir)

				'Extract modpack
				Try
					ZipFile.ExtractToDirectory(ofd.FileName, extractdir)
				Catch ex As Exception
					If TypeOf ex Is InvalidDataException Then
						MessageBox.Show("The specified ZIP Archive is not valid.", "Invalid ZIP", MessageBoxButtons.OK, MessageBoxIcon.Error)
					End If
					Return
				End Try

				'Find config.ini - always needed
				Dim inifound = Directory.EnumerateFiles(extractdir, "config.ini", SearchOption.AllDirectories)

				If inifound.Count = 0 Then
					MessageBox.Show("The specified ZIP Archive is not a valid modpack (config.ini not found).", "Invalid Modpack", MessageBoxButtons.OK, MessageBoxIcon.Error)
					Return
				End If

				'TODO: Consider multiple config.inis existing in modpack
				Dim configfile As New FileInfo(inifound(0))
				If configfile.Directory.Name = "config" Then
					'Check if not already existing mod
					Dim moddir = configfile.Directory.Parent

					For Each mp As ModPack In modpack.Items
						If mp.InternalName = moddir.Name Then
							'TODO: Easy way to edit modpack name this in the program.
							MessageBox.Show("A modpack with the same name already exists.", "Modpack already exists", MessageBoxButtons.OK, MessageBoxIcon.Error)
							Return
						End If
					Next

					'Copy parent's parent directory
					My.Computer.FileSystem.CopyDirectory(moddir.FullName, Path.Combine(DirManager.CustomThroneDirectory, moddir.Name))

					'Reload mod list
					ReloadModPacks()
				Else
					MessageBox.Show("The specified modpack has an invalid config.ini.", "Invalid Modpack", MessageBoxButtons.OK, MessageBoxIcon.Error)
					Return
				End If
			End If
		End Using
	End Sub

	Private Sub ButtonDetect_Click() Handles ButtonDetect.Click
		Dim foundfolder = DetectNTFolder()
		If foundfolder Is Nothing Then
			MessageBox.Show("Custom Throne Launcher was not able to detect your installation of Nuclear Throne." & vbCrLf & vbCrLf &
							"You will need to select it manually.", "Could not detect NT folder", MessageBoxButtons.OK, MessageBoxIcon.Information)
		Else
			'Set to found folder
			NTFolder.Text = foundfolder
		End If
	End Sub

	Private Sub ButtonBrowse_Click(sender As Object, e As EventArgs) Handles ButtonBrowse.Click
		'Browse for NT folder
		Using ofd As New OpenFileDialog
			ofd.AddExtension = False
			ofd.CheckFileExists = False
			ofd.CheckPathExists = True
			ofd.InitialDirectory = NTFolder.Text
			ofd.Filter = "nuclearthrone.exe|nuclearthrone.exe"
			ofd.Title = "Select Nuclear Throne Executable"
			If ofd.ShowDialog = DialogResult.OK Then
				NTFolder.Text = Path.GetDirectoryName(ofd.FileName)
			End If
		End Using
	End Sub

	Private Sub ListModPacks_ItemCheck(sender As Object, e As ItemCheckEventArgs) Handles ListModPacks.ItemCheck
		'Ignore initial check and unchecking on switch
		If ListModPacks.CheckedItems.Count = 0 OrElse ListModPacks.HideSelection Then Return

		If e.NewValue = CheckState.Unchecked Then
			'Undo unchecking, since this is not allowed
			e.NewValue = CheckState.Checked
		Else
			'Uncheck currently checked entry - use property as a "forced check" to ignore the code above
			ListModPacks.HideSelection = True
			ListModPacks.CheckedItems.Item(0).Checked = False
			ListModPacks.HideSelection = False

			'Update active mod entry in dropdown
			modpack.SelectedItem = GetModPack(ListModPacks.Items.Item(e.Index).Name)
		End If
	End Sub

	Private Sub modpack_SelectedIndexChanged(sender As Object, e As EventArgs) Handles modpack.SelectedIndexChanged
		'Update check on listview
		ListModPacks.Items.Item(DirectCast(modpack.SelectedItem, ModPack).InternalName).Checked = True
	End Sub

	' Update Check

	''' <summary>
	''' Checks for updates.
	''' </summary>
	''' <param name="apply">Whether to auto-apply the update when there is one.</param>
	Private Sub UpdateCheck(apply As Boolean)
		Try
			updatechecking = True

			LaunchPending.Close()

			StatusText.Text = "Checking for updates..."
			ButtonUpdate.Enabled = False
			StatusProgress.Style = ProgressBarStyle.Marquee
			BWupdate.RunWorkerAsync(apply)
		Catch ex As Exception
			ShowErrorMessage(ex, "An error occured initializing the update check!")
		End Try
	End Sub

	''' <summary>
	''' Cancels an ongoing update check
	''' </summary>
	Private Sub CancelUpdateCheck()
		If updatechecking Then
			'Cancel BW Task + WC action
			BWupdate.CancelAsync()
			dlclient.CancelAsync()

			'Reset UI + tell it to cancel the download
			ResetUpdateStatus()
		End If
	End Sub

	Private Sub BWupdate_DoWork(sender As Object, e As DoWorkEventArgs) Handles BWupdate.DoWork
		Try
			'Check for update - load text file with info
			dlclient = New TimedWebClient()
			Dim verinfo = dlclient.DownloadString(versionURL)

			If BWupdate.CancellationPending Then Return

			If verinfo IsNot Nothing AndAlso verinfo <> "" Then
				Dim version = verinfo.Split(vbCrLf)(0)
				Dim dllink = verinfo.Split(vbCrLf)(1)
				Dim snapversion = verinfo.Split(vbCrLf)(2)
				Dim snapdllink = verinfo.Split(vbCrLf)(3)

				e.Result = New UpdateResult(version, dllink, snapversion, snapdllink, e.Argument)
			End If

		Catch ex As Exception
			ShowErrorMessage(ex, "An error occured getting version info!")
		End Try
	End Sub

	Private Sub BWupdate_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles BWupdate.RunWorkerCompleted
		Try

			If e.Result Is Nothing Then
				'Finished work
				ResetUpdateStatus()
				Return
			End If

			'Save settings first
			ApplySettings()

			'Figure out action
			Dim result As UpdateResult = e.Result
			Dim newversion, newlink As String
			Dim issnapshot As Boolean

			If My.Settings.earlyaccess AndAlso result.SnapshotVersion <> My.Settings.latestsnapshot AndAlso result.SnapshotVersion <> My.Settings.latestversion Then
				'Only Update If newer snapshot And current, If using snapshots
				newversion = result.SnapshotVersion
				newlink = result.SnapshotDownload
				issnapshot = True
			ElseIf result.LatestVersion <> My.Settings.latestversion Then
				'Only Update If more recent, If Not using snapshots (override snapshots in case they are bugged)
				newversion = result.LatestVersion
				newlink = result.DownloadLink
				issnapshot = False
			Else
				'No update, return
				Return
			End If

			If Not result.ApplyUpdate Then
				'Prompt user first
				Dim yourversion = My.Settings.latestversion
				If issnapshot Then
					yourversion = My.Settings.latestsnapshot
				End If
				If yourversion = "" Then yourversion = "none"
				If MessageBox.Show("A newer version of Custom Throne is available (" & newversion & ", you have " & yourversion & ")." & vbCrLf & vbCrLf &
								   "Would you like to download and apply this update?", "Update?",
								   MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = DialogResult.No Then
					'Reset interface
					ResetUpdateStatus()
					Return
				End If
			End If

			'Update latest version setting
			If issnapshot Then
				My.Settings.latestsnapshot = newversion
			Else
				My.Settings.latestversion = newversion
			End If

			'Download and apply the update
			StatusText.Text = "Downloading update..."
			StatusProgress.Style = ProgressBarStyle.Continuous

			dlloc = My.Computer.FileSystem.GetTempFileName()
			dlclient = New TimedWebClient()
			AddHandler dlclient.DownloadProgressChanged, AddressOf DownloadProgressChanged
			AddHandler dlclient.DownloadFileCompleted, AddressOf DownloadFileCompleted
			dlclient.DownloadFileAsync(New Uri(newlink), dlloc)
		Catch ex As Exception
			ShowErrorMessage(ex, "An error occured initializing the Custom Throne download!")
			ResetUpdateStatus()
		End Try
	End Sub

	Private Sub DownloadProgressChanged(sender As Object, e As Net.DownloadProgressChangedEventArgs)
		'Update progress bar if different percentage
		If e.ProgressPercentage <> StatusProgress.Value Then
			StatusText.Text = "Downloading update... (" & e.ProgressPercentage & "%)"
			StatusProgress.Value = e.ProgressPercentage
		End If
	End Sub

	Private Sub DownloadFileCompleted(sender As Object, e As AsyncCompletedEventArgs)
		Try
			StatusText.Text = "Extracting archive..."
			StatusProgress.Style = ProgressBarStyle.Marquee

			'Extract ZIP
			Dim extractdir = DirManager.TempDirectory

			'Create dir in case it doesn't exist (otherwise exception)
			Directory.CreateDirectory(extractdir)

			'Extract archive
			ZipFile.ExtractToDirectory(dlloc, extractdir)

			'Remove files with same name - then move over
			For Each fullfl In Directory.EnumerateFiles(extractdir)
				Dim fl = fullfl.Replace(extractdir, "")
				File.Delete(Path.Combine(My.Settings.ntfolder, fl))
				File.Move(fullfl, Path.Combine(My.Settings.ntfolder, fl))
			Next

			'Remove remaining files
			File.Delete(dlloc)
			Directory.Delete(extractdir)

		Catch ex As Exception
			ShowErrorMessage(ex, "An error occured extracting the archive!")
		End Try

		'Reset interface
		ResetUpdateStatus()
	End Sub

	Private Sub ResetUpdateStatus()
		StatusText.Text = "Ready"
		StatusProgress.Style = ProgressBarStyle.Continuous
		ButtonUpdate.Enabled = True
		updatechecking = False

		If launchwhendone Then
			ButtonLaunch_Click()
		End If
	End Sub

	Private Sub NotifyIcon_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles NotifyIcon.MouseDoubleClick
		'Show window again
		Me.Show()
		NotifyIcon.Visible = False
	End Sub

	' Settings Tab

	Private Sub ApplySettingsManually() Handles ButtonSaveSettings.Click
		ApplySettings(True)
	End Sub

	Private Sub ApplySettings(Optional manualsave As Boolean = False)
		Try
			'Check validity of NT directory
			If manualsave AndAlso Not DirManager.ValidNTFolder(NTFolder.Text) Then
				'Warn user about this
				Tabs.SelectTab("TabSettings")
				MessageBox.Show("The specified Nuclear Throne folder is invalid (does not exist or does not contain NuclearThrone.exe)!", "Invalid NT Folder", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
			End If

			'Save every setting
			For Each ctrl As Control In TabSettings.Controls
				If ctrl.GetType Is GetType(TextBox) Then
					Dim newctrl = DirectCast(ctrl, TextBox)
					My.Settings.Item(newctrl.Name) = newctrl.Text
				ElseIf ctrl.GetType Is GetType(CheckBox) Then
					Dim newctrl = DirectCast(ctrl, CheckBox)
					My.Settings.Item(newctrl.Name) = newctrl.Checked
				ElseIf ctrl.GetType Is GetType(ComboBox) Then
					Dim newctrl = DirectCast(ctrl, ComboBox)
					My.Settings.Item(newctrl.Name) = newctrl.SelectedIndex
				End If
			Next

			If modpack.SelectedItem IsNot Nothing Then
				My.Settings.modpack = DirectCast(modpack.SelectedItem, ModPack).InternalName
			End If

			'Close/Open Steam connection
			If My.Settings.SteamInGame Then
				'Should show in-game
				InitSteamAPI()
			Else
				'Don't show as in-game
				'TODO: This does not stop the in-game message as of now!
				SteamAPI.Shutdown()
			End If

		Catch ex As Exception
			ShowErrorMessage(ex, "An error occured saving the settings!")
		End Try
	End Sub

	Private Sub InitSteamAPI()
		If SteamAPI.Init() Then
			username = SteamFriends.GetPersonaName()
			StatusUser.Text = "Logged in as " & username

			If Not My.Settings.SteamInGame Then
				'Close Steam
				SteamAPI.Shutdown()
			End If
		End If
	End Sub

	''' <summary>
	''' Reverts all settings in the TabSettings to their current value.
	''' </summary>
	Private Sub LoadSettings()
		Try
			For Each ctrl As Control In TabSettings.Controls
				If ctrl.GetType Is GetType(TextBox) Then
					Dim newctrl = DirectCast(ctrl, TextBox)
					newctrl.Text = My.Settings.Item(newctrl.Name)
				ElseIf ctrl.GetType Is GetType(CheckBox) Then
					Dim newctrl = DirectCast(ctrl, CheckBox)
					newctrl.Checked = My.Settings.Item(newctrl.Name)
				ElseIf ctrl.GetType Is GetType(ComboBox) Then
					Dim newctrl = DirectCast(ctrl, ComboBox)
					newctrl.SelectedIndex = My.Settings.Item(newctrl.Name)
				End If
			Next

		Catch ex As Exception
			ShowErrorMessage(ex, "An error occured loading the settings!")
		End Try
	End Sub

	' Mod Pack Functions

	''' <summary>
	''' Creates the folders for the modpack.
	''' </summary>
	''' <param name="mp">The modpack to create the folders for.</param>
	Private Sub CreateModFolders(mp As ModPack)
		'Don't create folders if no NT folder is detected
		If Not DirManager.ValidNTFolder() Then Return

		'Create all required folders
		For Each subdir In dirlist
			Directory.CreateDirectory(Path.Combine(mp.FullPath, subdir))
		Next
	End Sub

	''' <summary>
	''' Returns the ModPack object by its folder or display name.
	''' </summary>
	''' <param name="modname">Name to search for.</param>
	''' <param name="displayname">Check the display name instead of the folder name.</param>
	''' <returns></returns>
	Private Function GetModPack(modname As String, Optional displayname As Boolean = False) As ModPack
		For Each mp As ModPack In modpack.Items
			If displayname Then
				'Check displayname
				If mp.DisplayName = modname Then
					Return mp
				End If
			Else
				'Check foldername
				If mp.InternalName = modname Then
					Return mp
				End If
			End If
		Next

		Throw New ModNotFoundException("The mod """ & modname & """ was not found.")
	End Function

	Private Function ModPackExists(modname As String, Optional displayname As Boolean = False) As Boolean
		Try
			Dim mp = GetModPack(modname, displayname)
			Return True
		Catch ex As Exception
			Return False
		End Try
	End Function

	''' <summary>
	''' Reloads the lists of modpacks.
	''' </summary>
	Private Sub ReloadModPacks() Handles ButtonReloadMPs.Click, ReloadToolStripMenuItem.Click
		Try
			'Save settings beforehand
			ApplySettings()

			'Save selected modpack
			Dim activemodpack As ModPack = modpack.SelectedItem

			'Clear list first
			modpack.Items.Clear()
			ListModPacks.Items.Clear()

			'Create default entry
			modpack.Items.Add(New ModPack("default", "(Default)"))
			ListModPacks.Items.Add(NewModPackLVI("default", "(Default)", "../customthrone/default", "UNKNOWN"))

			'If Not DirManager.ValidNTFolder() Then Return

			'Create CT dir if not exists
			Directory.CreateDirectory(DirManager.CustomThroneDirectory)

			'Load all modpacks in folder
			For Each subfold In Directory.EnumerateDirectories(DirManager.CustomThroneDirectory)
				Dim subname = New DirectoryInfo(subfold).Name

				If subname <> "default" Then
					'Load metadata from config.ini
					Dim modcfg = Path.Combine(subfold, "config\config.ini")
					If File.Exists(modcfg) Then
						Dim name = subname
						Dim author = ""

						For Each ln In File.ReadAllLines(modcfg)
							If ln.StartsWith("name=") Then
								name = ln.Substring(5)
							ElseIf ln.StartsWith("author=") Then
								author = ln.Substring(7)
							End If
						Next

						modpack.Items.Add(New ModPack(subname, name))
						ListModPacks.Items.Add(NewModPackLVI(subname, name, "../customthrone/" & subname, author))
					Else
						'Just use internal name and give no extra infos
						modpack.Items.Add(New ModPack(subname, subname))
						ListModPacks.Items.Add(NewModPackLVI(subname, subname, "../customthrone/" & subname, "UNKNOWN"))
					End If
				End If
			Next

			'Re-select mod pack
			If activemodpack Is Nothing OrElse Not ModPackExists(activemodpack.InternalName) Then
				'Set to default
				activemodpack = GetModPack("default")
			End If

			modpack.SelectedItem = GetModPack(activemodpack.InternalName)
			ListModPacks.Items.Item(activemodpack.InternalName).Checked = True
		Catch ex As Exception
			ShowErrorMessage(ex, "An error occured reloading the modpack list!")
		End Try
	End Sub

	''' <summary>
	''' Creates a ListViewItem with the specified modpack infos.
	''' </summary>
	''' <param name="subitems"></param>
	''' <returns></returns>
	Private Function NewModPackLVI(internalname As String, ParamArray subitems As String()) As ListViewItem
		Dim lvi As New ListViewItem()
		lvi.Name = internalname
		For i = 0 To subitems.Count - 1
			lvi.SubItems.Add(subitems(i))
		Next
		Return lvi
	End Function

	''' <summary>
	''' Returns a path to the Nuclear Throne folder, if any was found. Else returns Nothing.
	''' </summary>
	''' <returns></returns>
	Private Function DetectNTFolder() As String
		Dim folder As String

		'1) Check current folder
		folder = Application.StartupPath
		If DirManager.ValidNTFolder(folder) Then Return folder

		'2) Check steam per registry
		Dim steamdir = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\Valve\Steam", "SteamPath", Nothing)

		If steamdir IsNot Nothing Then
			folder = Path.Combine(steamdir, "SteamApps\common\Nuclear Throne\")
			If DirManager.ValidNTFolder(folder) Then Return folder

			'3) Check other libraries (config.vdf in steam)
			Dim cfgfile = Path.Combine(steamdir, "config/config.vdf")
			If File.Exists(cfgfile) Then
				'Check BaseInstallFolder
				For Each ln In File.ReadAllLines(cfgfile)
					If ln.Contains("BaseInstallFolder") Then
                        'Get start of last string
                        Dim index = ln.LastIndexOf("""", ln.Length - 2)
						folder = (Path.Combine(ln.Substring(index).Replace("""", "").Replace("\\", "\"), "SteamApps\common\Nuclear Throne\"))
						If DirManager.ValidNTFolder(folder) Then Return folder
					End If
				Next
			End If
		End If

		Return Nothing
	End Function
End Class
