﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class NewMPaction
	Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		Try
			If disposing AndAlso components IsNot Nothing Then
				components.Dispose()
			End If
		Finally
			MyBase.Dispose(disposing)
		End Try
	End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
	Private Sub InitializeComponent()
		Me.components = New System.ComponentModel.Container()
		Me.Label1 = New System.Windows.Forms.Label()
		Me.ButtonOK = New System.Windows.Forms.Button()
		Me.ButtonCancel = New System.Windows.Forms.Button()
		Me.ModName = New System.Windows.Forms.TextBox()
		Me.ToolTip = New System.Windows.Forms.ToolTip(Me.components)
		Me.ModDisplay = New System.Windows.Forms.TextBox()
		Me.ModAuthor = New System.Windows.Forms.TextBox()
		Me.ModSource = New System.Windows.Forms.ComboBox()
		Me.Label2 = New System.Windows.Forms.Label()
		Me.Label3 = New System.Windows.Forms.Label()
		Me.Label4 = New System.Windows.Forms.Label()
		Me.Label5 = New System.Windows.Forms.Label()
		Me.SuspendLayout()
		'
		'Label1
		'
		Me.Label1.AutoSize = True
		Me.Label1.Location = New System.Drawing.Point(8, 38)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(38, 13)
		Me.Label1.TabIndex = 0
		Me.Label1.Text = "Name:"
		'
		'ButtonOK
		'
		Me.ButtonOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.ButtonOK.ForeColor = System.Drawing.SystemColors.WindowFrame
		Me.ButtonOK.Location = New System.Drawing.Point(96, 156)
		Me.ButtonOK.Name = "ButtonOK"
		Me.ButtonOK.Size = New System.Drawing.Size(75, 23)
		Me.ButtonOK.TabIndex = 1
		Me.ButtonOK.Text = "OK"
		Me.ButtonOK.UseVisualStyleBackColor = True
		'
		'ButtonCancel
		'
		Me.ButtonCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.ButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
		Me.ButtonCancel.ForeColor = System.Drawing.SystemColors.WindowFrame
		Me.ButtonCancel.Location = New System.Drawing.Point(176, 156)
		Me.ButtonCancel.Name = "ButtonCancel"
		Me.ButtonCancel.Size = New System.Drawing.Size(75, 23)
		Me.ButtonCancel.TabIndex = 2
		Me.ButtonCancel.Text = "Cancel"
		Me.ButtonCancel.UseVisualStyleBackColor = True
		'
		'ModName
		'
		Me.ModName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
			Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.ModName.Location = New System.Drawing.Point(60, 36)
		Me.ModName.Name = "ModName"
		Me.ModName.Size = New System.Drawing.Size(186, 20)
		Me.ModName.TabIndex = 3
		Me.ToolTip.SetToolTip(Me.ModName, "Enter the internal name of the mod. Try to avoid special characters.")
		'
		'ModDisplay
		'
		Me.ModDisplay.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
			Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.ModDisplay.Location = New System.Drawing.Point(60, 64)
		Me.ModDisplay.Name = "ModDisplay"
		Me.ModDisplay.Size = New System.Drawing.Size(186, 20)
		Me.ModDisplay.TabIndex = 5
		Me.ToolTip.SetToolTip(Me.ModDisplay, "Enter the display name of the mod. This is shown in the list and in-game.")
		'
		'ModAuthor
		'
		Me.ModAuthor.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
			Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.ModAuthor.Location = New System.Drawing.Point(60, 92)
		Me.ModAuthor.Name = "ModAuthor"
		Me.ModAuthor.Size = New System.Drawing.Size(186, 20)
		Me.ModAuthor.TabIndex = 7
		Me.ToolTip.SetToolTip(Me.ModAuthor, "Enter the name of the author(s) of this mod")
		'
		'ModSource
		'
		Me.ModSource.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
			Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.ModSource.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.ModSource.FormattingEnabled = True
		Me.ModSource.Location = New System.Drawing.Point(60, 120)
		Me.ModSource.Name = "ModSource"
		Me.ModSource.Size = New System.Drawing.Size(186, 21)
		Me.ModSource.TabIndex = 9
		Me.ToolTip.SetToolTip(Me.ModSource, "If wanted, you can use another mod pack as the source of your own. If you wish to" &
		" release it, though, be sure to contact the mod creator for permission!")
		'
		'Label2
		'
		Me.Label2.AutoSize = True
		Me.Label2.Location = New System.Drawing.Point(8, 66)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(44, 13)
		Me.Label2.TabIndex = 4
		Me.Label2.Text = "Display:"
		'
		'Label3
		'
		Me.Label3.AutoSize = True
		Me.Label3.Location = New System.Drawing.Point(8, 94)
		Me.Label3.Name = "Label3"
		Me.Label3.Size = New System.Drawing.Size(41, 13)
		Me.Label3.TabIndex = 6
		Me.Label3.Text = "Author:"
		'
		'Label4
		'
		Me.Label4.AutoSize = True
		Me.Label4.Location = New System.Drawing.Point(8, 124)
		Me.Label4.Name = "Label4"
		Me.Label4.Size = New System.Drawing.Size(44, 13)
		Me.Label4.TabIndex = 8
		Me.Label4.Text = "Source:"
		'
		'Label5
		'
		Me.Label5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
			Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Label5.Location = New System.Drawing.Point(8, 8)
		Me.Label5.Name = "Label5"
		Me.Label5.Size = New System.Drawing.Size(240, 16)
		Me.Label5.TabIndex = 10
		Me.Label5.Text = "Create a new modpack"
		Me.Label5.TextAlign = System.Drawing.ContentAlignment.TopCenter
		'
		'NewMPaction
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(258, 186)
		Me.Controls.Add(Me.Label5)
		Me.Controls.Add(Me.ModSource)
		Me.Controls.Add(Me.Label4)
		Me.Controls.Add(Me.ModAuthor)
		Me.Controls.Add(Me.Label3)
		Me.Controls.Add(Me.ModDisplay)
		Me.Controls.Add(Me.Label2)
		Me.Controls.Add(Me.ModName)
		Me.Controls.Add(Me.ButtonCancel)
		Me.Controls.Add(Me.ButtonOK)
		Me.Controls.Add(Me.Label1)
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
		Me.Name = "NewMPaction"
		Me.ShowInTaskbar = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
		Me.Text = "Create Modpack"
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub

	Friend WithEvents Label1 As Label
	Friend WithEvents ButtonOK As Button
	Friend WithEvents ButtonCancel As Button
	Friend WithEvents ModName As TextBox
	Friend WithEvents ToolTip As ToolTip
	Friend WithEvents Label2 As Label
	Friend WithEvents ModDisplay As TextBox
	Friend WithEvents Label3 As Label
	Friend WithEvents ModAuthor As TextBox
	Friend WithEvents Label4 As Label
	Friend WithEvents ModSource As ComboBox
	Friend WithEvents Label5 As Label
End Class
