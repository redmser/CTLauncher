﻿Public Class NewMPaction
	Private Sub NewMPaction_Load(sender As Object, e As EventArgs) Handles MyBase.Load
		'Reset textboxes
		ModName.Text = ""
		ModDisplay.Text = ""
		ModAuthor.Text = ""

		'Load mod source list
		Dim nonemod As New ModPack("default", "(None)")

		ModSource.Items.Clear()
		ModSource.Items.Add(nonemod)
		ModSource.SelectedItem = nonemod

		For Each mp As ModPack In Main.modpack.Items
			If mp.InternalName <> "default" Then
				ModSource.Items.Add(mp)
			End If
		Next
	End Sub

	Private Sub ButtonOK_Click(sender As Object, e As EventArgs) Handles ButtonOK.Click
		'Verify info
		If ModName.Text = "" Then
			MessageBox.Show("No mod name has been specified.", "Error creating mod", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
			Return
		End If

		For Each mp As ModPack In Main.modpack.Items
			If mp.InternalName = ModName.Text Then
				MessageBox.Show("A mod with the same name already exists.", "Error creating mod", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
				Return
			End If
		Next

		'Close dialog
		Me.DialogResult = DialogResult.OK
		Me.Close()
	End Sub
End Class