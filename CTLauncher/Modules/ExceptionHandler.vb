﻿Module ExceptionHandler
	''' <summary>
	''' Shows an error message with the provided exception info.
	''' </summary>
	''' <param name="ex"></param>
	''' <param name="msg"></param>
	Public Sub ShowErrorMessage(ex As Exception, msg As String)
		Dim str As String

		Try
			If ex Is Nothing Then
                'Only message
                str = msg
			Else
				Dim st = New StackTrace(ex, True)
				str = msg & vbCrLf & vbCrLf & ex.Message & vbCrLf & vbCrLf & "Stacktrace: " & ex.StackTrace
			End If
		Catch 'Show default error text in case everything goes wrong
			str = "Unable to get error information!" & vbCrLf & vbCrLf &
			"I'd suggest restarting the launcher. (Please report what you did to cause this, anyway)"
		End Try

        'Create and show dialog
        Using diag = New ErrorDiag(str)
			diag.ShowDialog()
		End Using
	End Sub
End Module
